package service;

import entity.Hotel;
import entity.Location;

import java.util.HashMap;
import java.util.Map;

import static constant.Constant.LOC_DIFF;

/**
 * Created by Utsav Chanda on 8/3/2016.
 */
public class HotelService {

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public double distance(Location location1, Location location2, char unit) {
        double lat1 = location1.getLatitude();
        double lon1 = location1.getLongitude();
        double lat2 = location2.getLatitude();
        double lon2 = location2.getLongitude();

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        } else if (unit == 'M') {
            dist = dist * 1609;
        }
        return (dist);
    }

    public boolean compareStrings(String string1, String string2) {

        boolean flag = true;
        String[] stringArr1 = string1.split(" ");
        String[] stringArr2 = string2.split(" ");

        Map<String, Integer> map = new HashMap<String, Integer>();

        for (String str : stringArr1) {
            Integer i = 0;
            if (map.get(str) == null) {
                i = 1;
            } else {
                i = map.get(str);
                i++;
            }
            map.put(str, i);
        }

        for (String str : stringArr2) {
            if (map.get(str) == null) {
                return false;
            } else {
                map.put(str, map.get(str) - 1);
            }
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (map.get(entry.getKey()) != 0)
                return false;
        }

        return flag;
    }

    public boolean compareHotelObjects(Hotel hotel1, Hotel hotel2) {

        boolean flag = true;

        if (hotel1 == null || hotel2 == null)
            return false;

        if (hotel1.equals(hotel2))
            return true;

        String hotelName1 = hotel1.getName();
        String hotelName2 = hotel2.getName();

        if (!(hotelName1.equalsIgnoreCase(hotelName2) || compareStrings(hotelName1.toLowerCase(), hotelName2.toLowerCase())))
            return false;

        Location location1 = hotel1.getLocation();
        Location location2 = hotel2.getLocation();
        if (!(location1.equals(location2)
                || distance(location1, location2, location1.getUnit()) <= LOC_DIFF)) {
            return false;
        }

        return flag;

    }

}









