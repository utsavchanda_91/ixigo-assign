package entity.enums;

/**
 * Created by Utsav Chanda on 8/3/2016.
 */
public enum EnumAmenities {

    BAR("Bar"),
    RESTAURANT("Restaurant"),
    WIFI("wifi"),
    AC("ac"),
    TV("tv"),
    GYM("gym"),
    PARKING("parking")
    ;

    String type;

    EnumAmenities(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EnumAmenities{" +
                "type='" + type + '\'' +
                '}';
    }
}
