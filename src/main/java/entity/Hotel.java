package entity;

import entity.enums.EnumAmenities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Utsav Chanda on 8/3/2016.
 */
public class Hotel {

    private String name;
    private String priceRange;
    private Location location;

    private Set<EnumAmenities> amenities = new HashSet<EnumAmenities>();
    private List<String> imageUrl = new ArrayList<String>();
    private List<String> reviews = new ArrayList<String>();

    public Hotel() {
    }

    public Hotel(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<EnumAmenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(Set<EnumAmenities> amenities) {
        this.amenities = amenities;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getReviews() {
        return reviews;
    }

    public void setReviews(List<String> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hotel)) return false;

        Hotel hotel = (Hotel) o;

        if (!name.equals(hotel.name)) return false;
        return location.equals(hotel.location);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + location.hashCode();
        return result;
    }
}
