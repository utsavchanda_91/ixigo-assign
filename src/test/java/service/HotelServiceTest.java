package service;

import entity.Hotel;
import entity.Location;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by Utsav Chanda on 8/3/2016.
 */
public class HotelServiceTest {

    HotelService hotelService = new HotelService();

    @Test
    public void testDistance() {
        Location location1 = new Location(28.4593, 77.0724, 'M');
        Location location2 = new Location(28.4592, 77.0723, 'M');
        double distance = hotelService.distance(location1, location2, 'M');
        assertTrue(distance < 100);
    }

    @Test
    public void testCompareStringsWhenStringsAreEqusl() {
        String hotelName1 = "Hotel Royal Madission Royal";
        String hotelName2 = "Royal Madission Hotel Royal";
        assertTrue(hotelService.compareStrings(hotelName1, hotelName2));
    }

    @Test
    public void testCompareStringsWhenStringsAreNotEqual() {
        String hotelName1 = "Hotel Royal Madission Royal";
        String hotelName2 = "Royal Madission Hotel";
        assertFalse(hotelService.compareStrings(hotelName1, hotelName2));
    }

    @Test
    public void testCompareObjectsWhenObjectsAreExactlyEqual(){
        String name = "Hotel Royal Madission";
        Location location = new Location(28.4593, 77.0724, 'M');
        Hotel hotel1 = new Hotel(name, location);
        Hotel hotel2 = new Hotel(name, location);
        assertTrue(hotelService.compareHotelObjects(hotel1, hotel2));
    }

    @Test
    public void testCompareObjectsWhenObjectsAreEqualWithVariationInLocation(){
        String name = "Hotel Royal Madission";
        Location location1 = new Location(28.45931, 77.0724, 'M');
        Location location2 = new Location(28.45932, 77.0724, 'M');
        Hotel hotel1 = new Hotel(name, location1);
        Hotel hotel2 = new Hotel(name, location2);
        assertTrue(hotelService.compareHotelObjects(hotel1, hotel2));
    }

    @Test
    public void testCompareObjectsWhenObjectsAreEqualWithVariationInName(){
        String name1 = "Hotel Royal Madission";
        String name2 = "Royal Madission Hotel";
        Location location = new Location(28.45931, 77.0724, 'M');
        Hotel hotel1 = new Hotel(name1, location);
        Hotel hotel2 = new Hotel(name2, location);
        assertTrue(hotelService.compareHotelObjects(hotel1, hotel2));
    }

    @Test
    public void testCompareObjectsWhenObjectsAreNotEqual(){
        String hotelName1 = "Hotel Le Merridian";
        String hotelName2 = "Hotel Royal Madission";
        Location location1 = new Location(28.4593, 77.0724, 'M');
        Location location2 = new Location(28.4594, 77.0725, 'M');
        Hotel hotel1 = new Hotel(hotelName1, location1);
        Hotel hotel2 = new Hotel(hotelName2, location2);
        assertFalse(hotelService.compareHotelObjects(hotel1, hotel2));
    }

}









